function JsonData2(value) {
  const data2 = value.filter(
    (data) => (data.gender === "female" || data.company === "FSW4") && data.age > 30
  );
  return data2;
}

const data = require('./data')
module.exports = JsonData2(data);