// Import Framework experess
const express = require("express");
const app = express();

// import data 
const allData = require("./public/script/data");

// import filter data 1
const filterData1 = require('./public/script/jsonData1')

// import filter data 2
const filterData2 = require('./public/script/jsonData2')

// import filter data 3
const filterData3 = require('./public/script/jsonData3')

// import filter data 4
const filterData4 = require('./public/script/jsonData4')

// import filter data 5
const filterData5 = require('./public/script/jsonData5')


// inisialiasi port yang digunakan
const PORT = 8000;

// set view engine untuk ejs
app.set("view engine", "ejs");

// menetapkan static file di public untuk css dan img
app.use(express.static("public"));



// routing root ke index.ejs
app.get("/", (req, res) => {
  res.status(200);
  res.render("index", {title: "Home Page"});
});

// routing root ke about.ejs
app.get("/about", (req, res) => {
  res.status(200);
  res.render("about", {title: "About Page"});
});

// menampilkan semua data
app.get("/data", (req, res) => {
  res.status(200);
  res.render("table1", {title: "Table Page", data: allData});
});

// menampilkan data filter 1
app.get("/data/datafilter1", (req, res) => {
  res.status(200);
  res.render("table1", {title: "Table Page", data: filterData1});
});

// menampilkan data filter 2
app.get("/data/datafilter2", (req, res) => {
  res.status(200);
  res.render("table1", {title: "Table Page", data: filterData2});
});

// menampilkan data filter 3
app.get("/data/datafilter3", (req, res) => {
  res.status(200);
  res.render("table1", {title: "Table Page", data: filterData3});
});

// menampilkan data filter 4
app.get("/data/datafilter4", (req, res) => {
  res.status(200);
  res.render("table1", {title: "Table Page", data: filterData4});
});

// menampilkan data filter 5
app.get("/data/datafilter5", (req, res) => {
  res.status(200);
  res.render("table1", {title: "Table Page", data: filterData5});
});


// routing root ke 404.ejs
app.use("/", (req, res) => {
  res.status(404);
  res.render("404");
});

// Jalankan server
app.listen(PORT, "0.0.0.0", () => {
  console.log("Server sudah berjalan, silahkan buka http://0.0.0.0:%d", PORT);
});
